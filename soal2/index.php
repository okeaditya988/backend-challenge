<?php

class Assert
{
    public static function cekHasil( $a, $b )
    {
        if ( $a != $b )
        {
            throw new Exception( 'Subjects are not equal.' );
        }
    }
}

class TestResult
{
    protected $_testableInstance = null;

    protected $_isSuccess = false;
    public function getSuccess()
    {
        return $this->_isSuccess;
    }

    protected $_output = '';
    public function getOutput()
    {
        return $_output;
    }
    public function setOutput( $value )
    {
        $_output = $value;
    }

    protected $_test = null;
    public function getTest()
    {
        return $this->_test;
    }

    public function getName()
    {
        return $this->_test->getName();
    }
    public function getComment()
    {
        return $this->ParseComment( $this->_test->getDocComment() );
    }

    private function ParseComment( $comment )
    {
        $lines = explode( "\n", $comment );
        for( $i = 0; $i < count( $lines ); $i ++ )
        {
            $lines[$i] = trim( $lines[ $i ] );
        }
        return implode( "\n", $lines );
    }

    protected $_exception = null;
    public function getException()
    {
        return $this->_exception;
    }

    static public function CreateFailure( Testable $object, ReflectionMethod $test, Exception $exception )
    {
        $result = new self();
        $result->_isSuccess = false;
        $result->testableInstance = $object;
        $result->_test = $test;
        $result->_exception = $exception;

        return $result;
    }
    static public function CreateSuccess( Testable $object, ReflectionMethod $test )
    {
        $result = new self();
        $result->_isSuccess = true;
        $result->testableInstance = $object;
        $result->_test = $test;

        return $result;
    }
}

abstract class Testable
{
    protected $test_log = array();
    protected function Log( TestResult $result )
    {
        $this->test_log[] = $result;

        printf( "Test: %s %s %s\n"
            ,$result->getName()
            ,$result->getSuccess() ? 'berhasil' : 'gagal'
            ,$result->getSuccess() ? '' : sprintf( "\n%s (lines:%d-%d; file:%s)"
                ,$result->getComment()
                ,$result->getTest()->getStartLine()
                ,$result->getTest()->getEndLine()
                ,$result->getTest()->getFileName()
                )
            );

    }
    final public function RunTests()
    {
        $class = new ReflectionClass( $this );
        foreach( $class->GetMethods() as $method )
        {
            $methodname = $method->getName();
            if ( strlen( $methodname ) > 4 && substr( $methodname, 0, 4 ) == 'Test' )
            {
                ob_start();
                try
                {
                    $this->$methodname();
                    $result = TestResult::CreateSuccess( $this, $method );
                }
                catch( Exception $ex )
                {
                    $result = TestResult::CreateFailure( $this, $method, $ex );
                }
                $output = ob_get_clean();
                $result->setOutput( $output );
                $this->Log( $result );
            }
        }
    }
}

class MyTest extends Testable
{
    function panjangString($string)
    {
        $panjangString = strlen($string);
        $j = 0;
        $i = 0;
        $maxPanjang = 0;
        $set = [];
        while ($j<$panjangString){
            if(array_key_exists($string[$j],$set)){
                $i = max($i,$set[$string[$j]]);

            }
            $maxPanjang = max($maxPanjang,$j-$i+1);
            $set[$string[$j]]=$j+1;
            $j++;
        }
        return $maxPanjang;
    }

    public function Test1()
    {
        Assert::cekHasil(6, $this->panjangString('charmander') );
    }

    public function Test2()
    {
        Assert::cekHasil(1, $this->panjangString('zzzzzzz') );
    }

    public function Test3()
    {
        Assert::cekHasil(3, $this->panjangString('xcchaar') );
    }

    public function Test4()
    {
        Assert::cekHasil(6, $this->panjangString('xcchaar') );
    }
}

$test = new MyTest();
$test->RunTests();
?>