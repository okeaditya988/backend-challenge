<?php

class Dispatcher
{

    private $request;

    public function dispatch()
    {
        $this->request = new Request();
        Router::parse($this->request->url, $this->request);
        $controller = $this->loadController();
        if(count($this->request->params)==0){
            call_user_func_array([$controller, $this->request->action], ['0' => null]);
        }elseif($_SERVER['REQUEST_METHOD']!="POST"){
            call_user_func_array([$controller, 'index'], $this->request->params);
        }
    }

    public function loadController()
    {
        $name = $this->request->controller . "Controller";
        $file = ROOT . 'Controllers/' . $name . '.php';
        require($file);
        $controller = new $name();
        return $controller;
    }

}
?>