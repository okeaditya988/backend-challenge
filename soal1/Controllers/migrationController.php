<?php
class migrationController extends Controller
{
    function index()
    {
        require(ROOT . 'Models/Migration.php');

        $migration = new Migration();

        $d['migration'] = $migration->migrate();
        $this->set($d);
        $this->render("index");
    }
}
?>