<?php
class authController extends Controller
{

    function index()
    {
        require(ROOT . 'Models/Auth.php');
        $auth= new Auth();

        $username = $_POST["username"];
        $password = $_POST["password"];

        $cek = $auth->cekAuth($username,$password);
        if($cek){
            $token = bin2hex(openssl_random_pseudo_bytes(64));
            if($auth->simpanToken($cek,$token)){
                $result = $token;
            }else{
                $result = "Gagal generate Token!";
            }
        }else{
            $result = "Username atau Password Salah";
        }
        echo json_encode($result);
    }
}
?>