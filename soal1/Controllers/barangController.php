<?php
class barangController extends Controller
{
    public function __construct()
    {
        require(ROOT . 'Models/Auth.php');
        require(ROOT . 'Models/Barang.php');
        $this->auth = new Auth();
        $this->barang= new Barang();
    }

    function getAuthorizationHeader()
    {
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        }else if (isset($_SERVER['HTTP_AUTHORIZATION'])) {
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        }elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            $requestHeaders = array_combine(array_map('ucwords', array_keys($requestHeaders)), array_values($requestHeaders));
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }

    function getBearerToken()
    {
        $headers = $this->getAuthorizationHeader();
        if (!empty($headers)) {
            if (preg_match('/Bearer\s(\S+)/', $headers, $matches)) {
                return $matches[1];
            }
        }
        return null;
    }

    function index($id)
    {
        $cek = $this->auth->cekToken($this->getBearerToken());
        if($cek){
            if ($_SERVER['REQUEST_METHOD'] === 'GET') {
                if($id){
                    $hasil = $this->showBarang($id);
                }else{
                    $hasil = $this->showAll();
                }
            }elseif($_SERVER['REQUEST_METHOD'] === 'POST') {
                $hasil = $this->simpan();
            }elseif($_SERVER['REQUEST_METHOD'] === 'PUT') {
                $hasil = $this->ubah($id);
            }elseif($_SERVER['REQUEST_METHOD'] === 'PATCH') {
                $hasil = $this->ubahPatch($id);
            }elseif($_SERVER['REQUEST_METHOD'] === 'DELETE') {
                $hasil = $this->hapus($id);
            }else{
                $hasil = 'Method Tidak Diketahui';
            }
        }else{
            $hasil = 'Autentikasi Token Salah';
        }
        echo json_encode($hasil);
    }

    function showAll()
    {
        $hasil = $this->barang->showAll();
        return $hasil;
    }

    function showBarang($id)
    {
        if (isset($id)){
            $hasil = $this->barang->showBarang($id);
            if($hasil==false){
                $hasil = 'Gagal Menampilkan Data Data';
            }
        }else{
            $hasil = 'Parameter Tidak Ditemukan';
        }
        return $hasil;
    }

    function simpan()
    {
        if (count($_POST!=2)){
            $hasil = $this->barang->simpan($_POST["namaBarang"], $_POST["jumlah"]);
            if($hasil==false){
                $hasil = 'Gagal Menyimpan Data';
            }else{
                $hasil = 'Berhasil Menyimpan Data';
            }
        }else{
            $hasil = 'Data Method Post Tidak Lengkap!';
        }
        return $hasil;
    }

    function ubah($id)
    {
        if (isset($id)){
            parse_str(file_get_contents("php://input"),$post_vars);
            if(count($post_vars)!=2){
                $hasil = 'Data Method Put Tidak Lengkap!';
            }else{
                $hasil = $this->barang->ubah($id, $post_vars["namaBarang"], $post_vars["jumlah"]);
                if($hasil==false){
                    $hasil = 'Gagal Memperbarui Data';
                }else{
                    $hasil = 'Berhasil Memperbarui Data';
                }
            }
        }else{
            $hasil = 'Parameter Tidak Ditemukan';
        }
        return $hasil;
    }

    function ubahPatch($id)
    {
        if (isset($id)){
            parse_str(file_get_contents("php://input"),$post_vars);
            $hasil = $this->barang->ubahPatch($id, $post_vars);
            if($hasil==false){
                $hasil = 'Gagal Memperbarui Data';
            }else{
                $hasil = 'Berhasil Memperbarui Data';
            }
        }else{
            $hasil = 'Parameter Tidak Ditemukan';
        }
        return $hasil;
    }

    function hapus($id)
    {
        if (isset($id)){
            $hasil = $this->barang->hapus($id);
            if($hasil==false){
                $hasil = 'Gagal Menghapus Data';
            }else{
                $hasil = 'Berhasil Menghapus Data';
            }
        }else{
            $hasil = 'Parameter Tidak Ditemukan';
        }
        return $hasil;
    }
}
