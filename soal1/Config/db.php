<?php

class Database
{
    //Atur Koneksinya Disini
    private static $username = "root";
    private static $password = "1155";
    private static $hostname = "localhost"; 
    private static $db = "test";
    private static $bdd = null;
    
    private function __construct() {
    }

    public static function getBdd() {
        try {
            self::$bdd = new PDO("mysql:host=".self::$hostname, self::$username, self::$password);
            self::$bdd->exec("CREATE DATABASE IF NOT EXISTS ".self::$db.";USE ".self::$db);
        } catch (PDOException $e) {
            if($e->getCode()=='2002'){
                echo 'Host MySQL salah. Silahkan cek kembali';
            }elseif($e->getCode()=='1045'){
                echo 'Akses Tidak Diperbolehkan. Silahkan cek username dan password MySQL anda';
            }elseif($e->getCode()=='1049'){
                echo 'Database'.self::$db.' tidak ditemukan. Buat baru atau Jalankan localhost/migration';
            }
        }
        return self::$bdd;
    }
}
?>