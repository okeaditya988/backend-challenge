<?php
class Barang extends Model
{

    public function showAll()
    {
        $sql = "SELECT * FROM barang";
        $req = Database::getBdd()->prepare($sql);
        $req->execute();
        return $req->fetchAll(PDO::FETCH_ASSOC);
    }

    public function showBarang($id)
    {
        $sql = "SELECT * FROM barang WHERE id = ?";
        $req = Database::getBdd()->prepare($sql);
        $req->execute([$id]);
        return $req->fetch(PDO::FETCH_ASSOC);
    }

    public function simpan($namaBarang, $jumlah)
    {
        $sql = "INSERT INTO barang (namaBarang, jumlah) VALUES (:namaBarang, :jumlah)";

        $req = Database::getBdd()->prepare($sql);

        return $req->execute([
            'namaBarang' => $namaBarang,
            'jumlah' => $jumlah
        ]);
    }

    public function ubah($id, $namaBarang, $jumlah)
    {
        $sql = "UPDATE barang SET namaBarang = :namaBarang, jumlah = :jumlah WHERE id = :id";

        $req = Database::getBdd()->prepare($sql);

        return $req->execute([
            'id' => $id,
            'namaBarang' => $namaBarang,
            'jumlah' => $jumlah

        ]);
    }

    public function ubahPatch($id, $array)
    {
        $set = array();
        foreach($array as $key => $value){
            $set[] = $key.'="'.$value.'"';
        }
        $set = implode(",",$set);
        $sql = "UPDATE barang SET $set WHERE id = ?";
        $req = Database::getBdd()->prepare($sql);
        return $req->execute([$id]);
    }

    public function hapus($id)
    {
        $sql = "DELETE FROM barang WHERE id = ?";
        $req = Database::getBdd()->prepare($sql);
        return $req->execute([$id]);
    }
}
?>