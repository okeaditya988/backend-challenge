<?php
class Migration extends Model
{

    public function cek($object,$jenis,$nama)
    {
        if($jenis == 'tabel'){
            if($object===0){
                echo "Berhasil membuat $jenis $nama<br>";
            }else{
                echo "Gagal membuat $jenis $nama : $object<br> ";
            }
        }else{
            if($object===1){
                echo "Berhasil membuat $jenis $nama<br>";
            }else{
                echo "Gagal membuat $jenis $nama : $object<br> ";
            }
        }
    }

    public function migrate()
    {
        $this->cek($this->create_users(),'tabel','user');
        $this->cek($this->create_barang(),'tabel','barang');
        $this->cek($this->seeder(),'seeder','semua tabel');
    }

    public function create_users()
    {
        $db = Database::getBdd();
        try {
            $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $sql ="DROP TABLE IF EXISTS user; CREATE TABLE user (
                id int(11) NOT NULL auto_increment, 
                username varchar(20) NOT NULL,
                password varchar(32) NOT NULL,
                token varchar(128) NULL,
                PRIMARY KEY (id),
                UNIQUE KEY username (username)
            );";
            $result = $db->exec($sql);
        } catch(PDOException $e) {
            $result = $e->getMessage();
        }
        return $result;
    }

    public function create_barang()
    {
        $db = Database::getBdd();
        try {
            $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $sql ="DROP TABLE IF EXISTS barang;CREATE TABLE barang (
                id int(11) NOT NULL auto_increment, 
                namaBarang varchar(20) NOT NULL,
                jumlah varchar(40) NOT NULL,
                PRIMARY KEY (id)
            );";
            $result = $db->exec($sql);
        } catch(PDOException $e) {
            $result = $e->getMessage();
        }
        return $result;
    }

    public function seeder()
    {
        $db = Database::getBdd();
        $sql = "";
        try {
            $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
            $sql .="INSERT INTO user(username,password) VALUES('admin',md5('password'));";
            $barang = ["Pensil" => 12,"Pena" => 6,"Buku" => 5,"Penghapus" => 9,"Penggaris" => 2];
            foreach($barang as $key=>$value){
                $sql .="INSERT INTO barang(namaBarang,jumlah) VALUES('$key','$value');";
            }
            $result = $db->exec($sql);
        } catch(PDOException $e) {
            $result = $e->getMessage();
        }
        return $result;
    }
}
?>