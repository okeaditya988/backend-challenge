<?php
class Auth extends Model
{
    public function cekAuth($username,$password)
    {
        $sql = "SELECT id FROM user WHERE username = :username AND password = :password";
        $req = Database::getBdd()->prepare($sql);
        $req->execute([
            'username' => $username,
            'password' => md5($password)
        ]);
        return $req->fetch()['id'];
    }

    public function simpanToken($id,$token)
    {
        $sql = "UPDATE user SET token = :token WHERE id = :id";

        $req = Database::getBdd()->prepare($sql);

        return $req->execute([
            'id' => $id,
            'token' => $token
        ]);
    }

    public function cekToken($token)
    {
        $sql = "SELECT id FROM user WHERE token = :token";
        $req = Database::getBdd()->prepare($sql);
        $req->execute([
            'token' => $token,
        ]);
        return $req->fetch()['id'];
    }
}
?>