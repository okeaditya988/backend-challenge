//Installation Guide

1. If using linux extract/copy file to directory www, if using xampp extract/copy file to directory htdocs
2. Run http://{nameServer}/migration
3. If success, you will see successful migration DB, Table and seeder

//Route
1.  POST - Login    : /auth
    Description: post username('admin') and password('password') to get Bearer Token
2.  GET - AllData   : /barang
    Description: using Bearer Token to get All Data Barang
3.  GET - Specific  : /barang/{id}      -> Ex: /barang/1
    Description: using Bearer Token to get Specific Data Barang using ID
4.  POST - Insert   : /barang
    Description: using Bearer Token + post nameBarang + post jumlah to Insert Data
5.  PUT - UpdateAll : /barang/{id}      -> Ex: /barang/1
    Description: using Bearer Token + nameBarang + jumlah to Update Data
6.  PATCH - Update : /barang/{id}      -> Ex: /barang/1
    Description: using Bearer Token + nameBarang / jumlah to Update Data
7.  DELETE - Remove : /barang/{id}      -> Ex: /barang/1
    Description: using Bearer Token to Remove Data

//Dokumentasi Postman
https://documenter.getpostman.com/view/10725182/SztG576A
https://www.getpostman.com/collections/31fb745e987d3d5ebe25